<?php

namespace Drupal\instagram_api_feeds\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings;
use InstagramScraper\Instagram;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Burst Instagram API feeds routes.
 */
class InstagramApiFeedsController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function get() {
    $account = \Drupal::request()->query->get('account', NULL);

    if (empty($account)) {
      return new JsonResponse('Please specify the account parameter.', Response::HTTP_BAD_REQUEST);
    }

    $accountOptions = $this->loadAccountOptions($account);
    if (empty($accountOptions)) {
      return new JsonResponse('Could not load account options.', Response::HTTP_BAD_REQUEST);
    }

    $data = $this->getInstagramData($account, $accountOptions);
    $response = new JsonResponse($data);
    return $response->setMaxAge(60);
  }

  /**
   * Loads stores options for account.
   *
   * @param string $account
   *   Name of Instagram account.
   */
  public function loadAccountOptions(string $account) {
    $config  = $this->config('instagram_api_feeds.settings');
    $options = $config->get('options');

    if (
      !is_array($options)
      || !array_key_exists($account, $options)
    ) {
      return NULL;
    }

    return $options[$account];
  }

  /**
   * Makes a request to Instagram and gets data.
   *
   * @param string $account
   *   Name of Instagram account.
   * @param array $options
   *   Array containing options for this account.
   */
  public function getInstagramData(string $account, array $options) {
    $cacheCid = "instagram_api_feeds.cache.$account";
    $data     = [];

    // Return cache if we have it.
    if ($cache = \Drupal::cache()->get($cacheCid)) {
      return $cache->data;
    }

    // Return Instagram data from PHP Instagram Scrape package.
    try {
      $instagram = new Instagram();

      // Use logged in method.
      if (
        $options["login"]
        && !empty($options["login"])
        && !empty($options["password"])
      ) {
        $instagram::withCredentials($options["login"], $options["password"], Settings::get('file_private_path'));
        \Drupal::logger('instagram_api_feeds')->notice(t('Using login session for Instagram request.'));
      }

      $excludedHashtags = preg_replace('/\s+/', '', $options["excluded_hashtags"]);
      $excludedHashtags = explode(',', $excludedHashtags);

      // Always fetch max in case we exclude posts by hashtag.
      $posts = $instagram->getMedias($account, 20);
      foreach ($posts as $post) {
        // If enough posts - end the loop.
        if (count($data) === (int) $options["posts_number"]) {
          break;
        }

        // Skip excluded hashtag.
        if ($this->findHashTags($post->getCaption(), $excludedHashtags)) {
          continue;
        }

        $data[] = [
          'key'     => $post->getShortCode(),
          'type'    => $post->getType(),
          'link'    => $post->getLink(),
          'media'   => [
            'high_resolution'  => $post->getImageHighResolutionUrl(),
            'square_thumbnail' => [
              '480' => $post->getSquareImages()[3] ?? '',
              '640' => $post->getImageThumbnailUrl(),
            ],
          ],
          'caption' => $post->getCaption(),
        ];

        // Set cache.
        \Drupal::cache()->set($cacheCid, $data,
          $this->calculateCacheLifetime((int) $options['instagram_cache_lifetime']));
      }
    }
    catch (\Exception $exception) {
      \Drupal::logger('instagram_api_feeds')->error($exception);

      // Set cache on error as well - prevents spamming Instagram.
      \Drupal::cache()->set($cacheCid, [], $this->calculateCacheLifetime());
    }

    return $data;
  }

  /**
   * Finds hash in the Instagram post caption text.
   *
   * @param string $caption
   *   Instagram post caption.
   * @param array $hashtags
   *   Array of hashtags.
   */
  public function findHashTags(string $caption, array $hashtags) {
    foreach ($hashtags as $hashtag) {
      if (
        !empty($hashtag)
        && strpos(mb_strtolower($caption), $hashtag) !== FALSE
      ) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Calculates cache lifetime for provided hours.
   *
   * @param int $lifeTimeInHours
   *   How long cache to last in hours.
   */
  public function calculateCacheLifetime(int $lifeTimeInHours = 24) {
    $cache_lifetime = \Drupal::time()->getCurrentTime() + ($lifeTimeInHours * 3600);

    return (int) $cache_lifetime;
  }

}
